fun main(args:Array<String>)
{
    print(isPalindrome("sos"))
}

fun isPalindrome(s: String): Boolean {
    return s == s.reversed()
}