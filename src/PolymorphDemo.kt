fun main(args: Array<String>) {
    val p1 = Point(300, 50)
    val p2 = Point(200, 30)

    var sum = Point()
    sum = p1 + p2

    println("sum = (${sum.run}, ${sum.matches})")
}

class Point(val run: Int = 0, val matches: Int = 0) {

    operator fun plus(p: Point) : Point {
        return Point(run + p.run, matches + p.matches)
    }
}