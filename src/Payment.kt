abstract class Payment internal constructor(`val`: Double) {
    protected var cash: Double
    fun getcash(): Double {
        return cash
    }

    fun setcash(newval: Double) {
        cash = newval
    }

    open fun paymentDetails() {
        println("The payment of cash: " + cash)
    }

    init {
        cash = Math.round(`val` * 100) / 100.0
    }
}

class CashPayment internal constructor(`val`: Double) : Payment(`val`) {
    override fun paymentDetails() {
        println("The payment of cash:  " + cash)
    }
}

class CreditCardPayment internal constructor(value: Double, var name: String, var expDate: String, var number: String) : Payment(value) {
    override fun paymentDetails() {
        println("The payment of " + cash + " through the card " + number
                + ",  and expire date " + expDate + ", and the owner name: " + name + ".")
    }

}

fun main(args: Array<String>) {
    val x = CreditCardPayment(2.12, "Sita", "11/27", "************1234")
    val p = CashPayment(20.03)
    val y = CreditCardPayment(11.22, "Gita", "11/25", "987654321")
    val q = CashPayment(55.12)
    x.paymentDetails()
    p.paymentDetails()
    y.paymentDetails()
    q.paymentDetails()
}
