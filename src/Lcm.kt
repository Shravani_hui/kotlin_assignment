fun main(arg:Array<String>)
{
        var number1:Int = 72
        var number2:Int = 120
        var lcm_temp:Int

    /**
     * We have checked this because LCM should be greater than the maximum number
     */
        if (number1>number2)
        {
            lcm_temp = number1
        }
    else
        {
            lcm_temp = number2
        }

        while (true) {
            if (lcm_temp % number1 == 0 && lcm_temp % number2 == 0) {
                println("LCM of $number1 and $number2 is: $lcm_temp")
                break
            }
            /**
             * If lcm not found then keep incrementing the maximum number
             */
            lcm_temp++

        }

}