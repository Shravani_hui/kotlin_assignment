data class Players(
        val runs: Int,
        val age: Int,
        val nom: Boolean,
        val name: String,
        val player_number: Int
)

fun sendData(): Players {
    return Players(10106, 38, true, "Sachin Tendulkar", 10)
}

fun main() {

    val map = mutableMapOf<Int, Players>()
    map.put(1, Players(2220, 21, false, "Pandya", 123))
    map.put(2, Players(500, 25, true, "Harbhajan", 112))
    map.put(3, Players(3100, 23, true, "Kohli", 111))
    map.put(4, Players(3221, 23, true, "Raina", 114))
    map.put(5, Players(3800, 40, false, "Ashwin", 117))

    println("Print All List with key value pair")
    println(map)
    println("")
    val newmap = map.get(4)
    println("Print All values of player number 4")
    println(newmap)
    println("")


    val (runs, age, nom, name, player_number) = sendData()
    println("Name is $name")
    println("Age is $age")
    println("player_number is $player_number")
    println("nom is $nom")
    println("runs is $runs")

}