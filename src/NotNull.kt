import java.util.*

fun main(args:Array<String>)
{
    val numbers: List<Int?> = listOf(1, 2, null, 4)
    val nonNullNumbers = numbers.filterNotNull()
    val evenNumbers = nonNullNumbers.filter{ it % 2 == 0 }.toTypedArray()

    println("Non null and even arraylist is ${Arrays.toString(evenNumbers)}")


}

