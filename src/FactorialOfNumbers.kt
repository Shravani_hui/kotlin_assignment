fun main(args:Array<String>)
{
    var number = 5
    var factorial_Of_num = factorial(number)
    println("Factorial of number $number is: $factorial_Of_num")
}

fun factorial(num:Int):Int
{
    if(num==1 || num==0)
    {
        return 1
    }
    else
    {
        return num*factorial(num-1)
    }
}