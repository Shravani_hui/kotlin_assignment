fun main(args:Array<String>)
{
    val a = -5 + 8 * 6

    val b = (55 + 9) % 9

    val c = 20 + -3 * 5 / 8

    val d = 5 + 15 / 3 * 2 - 8 % 3

    println("Print value of a: $a")
    println("Print value of b: $b")
    println("Print value of c: $c")
    println("Print value of d: $d")
}