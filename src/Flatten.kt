fun main()
{
    val listOfLists: List<List<Int?>> = listOf(listOf(1), listOf(2,3,null,4),listOf(null,5))

    val flattened: List<Int?> = listOfLists.flatten()
 
    val nonNullNumbers = flattened.filterNotNull()

    print(nonNullNumbers)
}
