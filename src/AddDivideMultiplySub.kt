fun main(args:Array<String>)
{
      var operation:String = "-"

    when(operation)
    {
        "+" -> print(add(30,5))
        "-" -> print(sub(30,5))
        "/" -> print(divide(30,5))
        "*" -> print(multiply(30,5))
        else -> print("No operation passed!!")
    }

}

fun add(a:Int,b:Int):Int
{
    return a.plus(b)
}

fun sub(a:Int,b:Int):Int
{
    return a.minus(b)
}

fun multiply(a:Int,b:Int):Int
{
    return a.times(b)
}

fun divide(a:Int,b:Int):Int
{
    return a.div(b)
}