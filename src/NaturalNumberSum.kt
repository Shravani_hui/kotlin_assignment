fun main(args:Array<String>)
{
    var number = 20
    var natural_num_sum = naturalNumberSum(number)
    print("Sum of number $number is $natural_num_sum")
}

fun naturalNumberSum(num:Int):Int
{
    if(num!=0)
    {
      return  num + naturalNumberSum(num-1)
    }
    else
    {
       return num
    }

}