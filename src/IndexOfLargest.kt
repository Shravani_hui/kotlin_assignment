fun main(args:Array<String>) {
    var array1 = intArrayOf(1, 21, 223, 97, 67)
    var temp = array1[0]

    println(indexOfMax(array1))

}

fun indexOfMax(a: IntArray): Int? {
    var maxIndex = 0
    for (elem in a.indices) {
        val newElem = a[elem]
        if (newElem >= a[maxIndex]) {
            maxIndex = elem;
        }
    }
    return maxIndex
}
