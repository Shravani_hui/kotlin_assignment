fun main(args:Array<String>)
{

    var arrayList = arrayListOf(5, 4, 3, 2, 1)
    var result = getReversedList(arrayList)

    print("$result")
}

fun  getReversedList(list:List<Int>):List<Int> {
    val originalReadOnly = list as List<Int>
    val reversed = originalReadOnly.asReversed()
    return reversed
}

