fun main(args: Array<String>) {
    val row = 4
    val column = 4
    val matrix = arrayOf(intArrayOf(1, 1, 1, 1), intArrayOf(2, 2, 2, 2), intArrayOf(3, 3, 3, 3), intArrayOf(4, 4, 4, 4))

    // Display current matrix
    display(matrix)

    // Transpose the matrix
    val transpose = Array(column) { IntArray(row) }
    for (i in 0..row - 1) {
        for (j in 0..column - 1) {
            transpose[j][i] = matrix[i][j]
        }
    }

    // Display transposed matrix
    display(transpose)
}

fun display(matrix: Array<IntArray>) {
    println("The matrix is: ")
    for (row in matrix) {
        for (column in row) {
            print("$column    ")
        }
        println()
    }
}