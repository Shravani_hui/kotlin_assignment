fun main(args:Array<String>)
{
    val savings = SavingsAccount(1000.00, 0.10)
    savings.withdraw(250.0)
    savings.deposit(400.0)
    savings.addInterest()
    println(savings.balance)
    println("Expected: 1265.0")
}




class SavingsAccount {
    var balance: Double
        private set
    private var interest: Double

    constructor() {
        balance = 0.0
        interest = 0.0
    }

    constructor(initialBalance: Double, initialInterest: Double) {
        balance = initialBalance
        interest = initialInterest
    }

    fun deposit(amount: Double) {
        balance = balance + amount
    }

    fun withdraw(amount: Double) {
        balance = balance - amount
    }

    fun addInterest() {
        balance = balance + balance * interest
    }

}